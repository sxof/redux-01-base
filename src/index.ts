import { createStore, Store } from 'redux';
import { counterReducer } from './counter/counter.reduce';
import {
    increaseAction,
    multiplyAction,
    resetAction
} from './counter/counter.actions';

const store: Store = createStore(counterReducer);

store.subscribe(() => {
    console.log('Subs:', store.getState());
});

store.dispatch(increaseAction);
store.dispatch(increaseAction);
store.dispatch(increaseAction);
store.dispatch(multiplyAction);
store.dispatch(resetAction);