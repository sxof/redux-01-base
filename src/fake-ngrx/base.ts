import { Observable, Subscriber } from 'rxjs';
export interface Action {
    type: string;
    payload?: any;
}

export interface Reducer<T> {
    (state: T, action: Action): T;
}

export class Store<T> extends Observable<void> {
    private emptySubs: Subscriber<void>;

    constructor(
        private reducer: Reducer<T>,
        private state?: T
    ) {
        super((subs: Subscriber<any>) => {
            this.emptySubs = subs;
        });
    }

    getState(): T {
        return this.state;
    }

    dispatch(action: Action): void {
        this.state = this.reducer(this.state, action);
        this.emptySubs.next();
    }
}