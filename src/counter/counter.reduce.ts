import { Action } from '../fake-ngrx/base';

export function counterReducer(state = 10, action: Action): any {
    switch (action.type) {
        case 'INCREASE':
            state++;
            break;
        case 'DECREASE':
            state--;
            break;
        case 'MULTIPLY':
            state = state * action.payload;
            break;
        case 'DIVIDE':
            state = state / action.payload;
            break;
        case 'RESET':
            state = 0;
            break;
    }
    return state;
}