interface Action<T = {}> {
    type: string;
    payload?: T;
}

function reducer<T = {}, K = {}>(state: T, action: Action<K>): T {
    switch (action.type) {
        case 'INCREASE':
            if (typeof state === 'number') {
                state++;
            }
            break;
        case 'DECREASE':
            if (typeof state === 'number') {
                state--;
            }
            break;
        case 'MULTIPLY':
            if (typeof state === 'number' && typeof action.payload === 'number') {
                state = <any>(state * action.payload);
            }
            break;
        case 'DIVIDE':
            if (typeof state === 'number' && typeof action.payload === 'number') {
                state = <any>(state / action.payload);
            }
            break;
    }
    return state;
}

const increaseAction: Action = { type: 'INCREASE' }
console.log(reducer(10, increaseAction));
const decreaseAction: Action = { type: 'DECREASE' }
console.log(reducer(10, decreaseAction));
const multiplyAction: Action<number> = { type: 'MULTIPLY', payload: 3 }
console.log(reducer(10, multiplyAction));
const divideAction: Action<number> = { type: 'DIVIDE', payload: 2 }
console.log(reducer(10, divideAction));