import { Store } from '../fake-ngrx/base';
import { counterReducer } from '../counter/counter.reduce';
import {
    increaseAction,
    multiplyAction,
    resetAction
} from '../counter/counter.actions';

const store = new Store(counterReducer);

store.subscribe(() => {
    console.log('Subs:', store.getState());
});

store.dispatch(increaseAction);
store.dispatch(increaseAction);
store.dispatch(increaseAction);
store.dispatch(multiplyAction);
store.dispatch(resetAction);