export { };
import {
    increaseAction,
    decreaseAction,
    multiplyAction,
    divideAction,
    resetAction
} from '../counter/counter.actions';
import { Action } from '../fake-ngrx/base';

function reducer<T = {}, K = {}>(state: T, action: Action): T {
    switch (action.type) {
        case 'INCREASE':
            if (typeof state === 'number') {
                state++;
            }
            break;
        case 'DECREASE':
            if (typeof state === 'number') {
                state--;
            }
            break;
        case 'MULTIPLY':
            if (typeof state === 'number' && typeof action.payload === 'number') {
                state = <any>(state * action.payload);
            }
            break;
        case 'DIVIDE':
            if (typeof state === 'number' && typeof action.payload === 'number') {
                state = <any>(state / action.payload);
            }
            break;
        case 'RESET':
            if (typeof state === 'number') {
                state = <any>0;
            }
            break;
    }
    return state;
}

console.log(reducer(10, increaseAction));
console.log(reducer(10, decreaseAction));
console.log(reducer(10, multiplyAction));
console.log(reducer(10, divideAction));
console.log(reducer(10, resetAction));